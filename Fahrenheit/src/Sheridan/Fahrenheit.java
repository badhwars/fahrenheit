package Sheridan;

public class Fahrenheit {

	public static void main(String args[]) {
		System.out.println(convertFromCelcius(100));
		System.out.println(convertFromCelcius(50));
		System.out.println(convertFromCelcius(75));
		System.out.println(convertFromCelcius(0));
		
	}

public static int convertFromCelcius(int a) {
	int ans = 0;
	double tempF = (a * 9)/5.0 + 32;
	tempF=(Math.ceil(tempF)); // round up
//	System.out.println(tempF);
	if(tempF > 0) {
	ans = (int)tempF;//
	}
	if (tempF < 0) {
		throw new IllegalArgumentException("Cant be negative");
	}
	
	return ans;
}
}
