package Sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
import Sheridan.Fahrenheit;
public class FahrenheitTest {

	Fahrenheit a = new Fahrenheit();
	@Test
	public void testConvertFromCelcius() {
	
		
		assertEquals(32,a.convertFromCelcius(0));
		assertEquals(212,a.convertFromCelcius(100));
		//assertFail(a.convertFromCelcius(-20));
		
		assertEquals(36,a.convertFromCelcius(2)); // boundry TEst//
		assertEquals(157,a.convertFromCelcius(69)); // boundry Test 
		assertEquals(128,a.convertFromCelcius(53)); //boundry Test to check roundUp
		
		
	}

}
